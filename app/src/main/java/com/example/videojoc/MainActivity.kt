package com.example.videojoc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.videojoc.ui.GameFragment
import com.example.videojoc.ui.MenuFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}