package com.example.videojoc.model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF
import com.example.videojoc.R

class Player(context: Context, val screenX: Int, val screenY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.spaceship)
    val width = screenX / 8f
    val height = screenY / 8f
    var positionX = screenX / 2
    var positionY = screenY - 300
    //
    var hitbox = RectF()

    init {
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)
    }
}

