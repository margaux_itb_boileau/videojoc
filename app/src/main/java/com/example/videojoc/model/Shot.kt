package com.example.videojoc.model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF
import com.example.videojoc.R

class Shot(context: Context, screenX: Int, screenY: Int, var positionX:Int, var positionY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.bullet)
    val width = screenX / 50f
    val height = screenY / 50f

    var speed = 25
    var hitbox = RectF()

    init {
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)
    }

    fun updateShot() {
        positionY -= speed
    }
}
