package com.example.videojoc.model

import android.content.Context
import android.graphics.*
import android.media.MediaPlayer
import android.media.SoundPool
import android.view.MotionEvent
import android.view.SurfaceView
import androidx.navigation.findNavController
import com.example.videojoc.R
import com.example.videojoc.ui.GameFragmentDirections
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class GameView(context: Context, private val size: Point) : SurfaceView(context) {
    //Ui View
    var canvas: Canvas = Canvas()
    val paint: Paint = Paint()
    //Elements
    val player = Player(context,size.x,size.y)
    val enemies = mutableListOf<Enemy>()
    val shots = mutableListOf<Shot>()
    //Media
    var mediaPlayer = MediaPlayer.create(context, R.raw.playing)
    val soundPool = SoundPool.Builder().setMaxStreams(5).build()
    val gameOver = soundPool.load(context, R.raw.dead, 0)
    val start = soundPool.load(context, R.raw.start, 0)
    val kill = soundPool.load(context, R.raw.kill, 0)
    val bonusSound = soundPool.load(context, R.raw.bonus, 0)
    //Delays
    var bonus:Boolean = false
    var kills = 0
    var playing = true
    var score = 0
    var touch = 0

    init {
        playSound(start)
        startGame()
        asteroids()
    }
    fun startGame(){
        CoroutineScope(Dispatchers.Main).launch{
            while(playing) {
                mediaPlayer.start()
                mediaPlayer.setOnCompletionListener { mediaPlayer.start() }
                draw()
                update()
                delay(20)
            }
            mediaPlayer.pause()
            playSound(gameOver)
            val action = GameFragmentDirections.actionGameFragmentToResultFragment(score)
            findNavController().navigate(action)
        }
    }

    fun asteroids() {
        CoroutineScope(Dispatchers.Main).launch {
            var delay = 2000L
            while (playing) {
                enemies.add(Enemy(context,size.x,size.y))
                delay(delay)
                if (delay != 500L) delay-=50
            }
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event != null) {
            if (touch%10==0) {
                shots.add(
                    Shot(
                        context,
                        size.x,
                        size.y,
                        player.positionX + (player.width / 2).toInt(),
                        player.positionY
                    )
                )
            }
            player.positionX = event.x.toInt()-(player.width/2).toInt()
            touch++
        }
        return true
    }

    fun draw(){
        if (holder.surface.isValid) {
            canvas = holder.lockCanvas()
            canvas.drawColor(Color.BLACK)
            canvas.drawBitmap(player.bitmap,player.positionX.toFloat(),player.positionY.toFloat(),paint)
            try {
                collisionCheck()
                for(enemy in enemies) {
                    canvas.drawBitmap(enemy.bitmap,enemy.positionX.toFloat(),enemy.positionY.toFloat(),paint)
                }
                for(shot in shots) {
                    canvas.drawBitmap(shot.bitmap,shot.positionX.toFloat(),shot.positionY.toFloat(),paint)
                }
                if(bonus) {
                    playSound(bonusSound)
                    var star = Bonus(context,size.x,size.y)
                    canvas.drawBitmap(star.bitmap,star.positionX.toFloat(),star.positionY.toFloat(),paint)
                }
                bonus=false
            }catch (_:java.util.ConcurrentModificationException){}

            holder.unlockCanvasAndPost(canvas)
        }
    }

    fun collisionCheck() {
        for (enemy in enemies) {
            if(RectF.intersects(enemy.hitbox,player.hitbox)) {
                playing = false
            }
            for (shot in shots){
                if (RectF.intersects(enemy.hitbox,shot.hitbox)){
                    playSound(kill)
                    kills++
                    enemies.remove(enemy)
                    if (kills%10==0) bonus=true
                    if (bonus) score+= 50
                    else score+=10
                }
            }
        }
    }

    fun update(){
        player.hitbox.top = player.positionY.toFloat()
        player.hitbox.bottom = player.hitbox.top+player.height
        player.hitbox.left = player.positionX.toFloat()
        player.hitbox.right = player.hitbox.left+player.width
        try{
            for(enemy in enemies){
                enemy.hitbox.top = enemy.positionY.toFloat()
                enemy.hitbox.bottom = enemy.hitbox.top+enemy.width
                enemy.hitbox.left = enemy.positionX.toFloat()
                enemy.hitbox.right = enemy.hitbox.left+enemy.width
                if(enemy.positionY<size.y) enemy.updateEnemy()
                else playing = false
            }
            for(shot in shots){
                shot.hitbox.top = shot.positionY.toFloat()
                shot.hitbox.bottom = shot.hitbox.top+shot.height
                shot.hitbox.left = shot.positionX.toFloat()
                shot.hitbox.right = shot.hitbox.left+shot.width
                if(shot.positionY>0) shot.updateShot()
                else shots.remove(shot)
            }
        }catch (_:java.util.ConcurrentModificationException){}
    }

    fun playSound(id: Int){
        soundPool.play(id, 1f, 1f, 0, 0, 1f)
    }



}
