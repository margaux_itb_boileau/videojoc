package com.example.videojoc.model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Point
import android.graphics.RectF
import android.view.View
import com.example.videojoc.R
import kotlin.random.Random


class Enemy(context: Context, screenX: Int, screenY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.asteroid2)
    val width = screenX / 10f
    val height = screenY / 15f
    var positionX = (-screenX/12..screenX-screenX/12).random()
    var positionY = -screenY
    val speed = (5..15).random()
    var hitbox = RectF()

    init{
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)
    }

    fun updateEnemy(){
        positionY+=speed
    }
}
