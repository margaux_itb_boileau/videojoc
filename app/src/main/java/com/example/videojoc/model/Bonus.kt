package com.example.videojoc.model

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.example.videojoc.R

class Bonus(context: Context, screenX: Int, screenY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.star_bonus)
    val width = screenX / 5f
    val height = screenY / 10f
    var positionX = screenX - 400
    var positionY = screenY / 10

    init {
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)
    }

}