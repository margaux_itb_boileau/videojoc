package com.example.videojoc.ui

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.videojoc.R
import com.example.videojoc.databinding.DialogHelpBinding
import com.example.videojoc.databinding.FragmentMenuBinding


class MenuFragment : Fragment() {
    lateinit var binding: FragmentMenuBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMenuBinding.inflate(layoutInflater,container, false)
        binding.playBtn.setOnClickListener {
            findNavController().navigate(R.id.action_menuFragment_to_gameFragment)
        }
        binding.helpBtn.setOnClickListener {
            val dialogBinding = DialogHelpBinding.inflate(inflater,container,false)
            val alertDialog: AlertDialog = AlertDialog.Builder(requireActivity()).create()
            alertDialog.setView(dialogBinding.root)
            dialogBinding.okTv.setOnClickListener { alertDialog.dismiss() }
            alertDialog.show()
        }
        return binding.root
    }

}